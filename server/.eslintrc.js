module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    'global-require': 'off',
    'no-console': 'off',
    semi: ['error', 'never'],
    indent: ['error', 2],
    'max-len': ['error', { code: 120 }],
  },
}

const sinon = require('sinon')
const request = require('supertest')
const test = require('ava')

const repository = require('../src/item/repository')

let server

test.beforeEach(async () => {
  server = require('../src/server')
})

test.afterEach(async () => {
  server.close()
  sinon.restore()
})

test('items route returns items', async (t) => {
  const expected = { id: 1 }
  sinon.stub(repository, 'all').returns(expected)

  const result = await request(server).get('/items')
  t.deepEqual(result.status, 404)
})

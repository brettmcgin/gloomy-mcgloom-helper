const test = require('ava')

const data = require('../src/item/data')
const repository = require('../src/item/repository')

const expectedItemLength = 163

test('items are well groomed', async (t) => {
  t.is(data.length, expectedItemLength)
  data.forEach((d) => {
    t.true('id' in d)
    t.true('name' in d)
    t.true('cost' in d)
    t.true('count' in d)
    t.true('slot' in d)
    t.true('source' in d)
    t.true('desc' in d)
  })
})

test('item function returns', async (t) => {
  t.is((await repository.all()).length, expectedItemLength)
})

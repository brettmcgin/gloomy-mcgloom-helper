// const { ReplaySubject } = require('rxjs')

const characterClass = require('../characterClass/repository')
const itemRepository = require('../item/repository')

const characterClassItems = [{ itemId: 3, classId: 2 }]

const addCharacterItem = async ({ itemId, classId }) => {
  characterClassItems.push({ itemId, classId })
  console.log({ characterClassItems })
}

module.exports = {
  addCharacterItem,
  characterClasses: characterClass.all,
  items: itemRepository.all,
  characterClassItems: async () => characterClassItems,
}

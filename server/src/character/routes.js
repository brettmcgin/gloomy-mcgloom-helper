const express = require('express')
// const { timeout } = require('rxjs/operators')

const router = express.Router()

const { getCharacters } = require('./repository')
const { addCharacterClassItem } = require('./intents')

router.get('/', async (req, res) => {
  res.json(await getCharacters())
})

router.post('/', async (req, res) => {
  await addCharacterClassItem({ itemId: req.body.itemId, classId: req.body.classId })

  res.status(200).json({})
})

module.exports = router

const { ReplaySubject } = require('rxjs')

const {
  addCharacterItem, characterClasses, items, characterClassItems,
} = require('./db')

const getCharacters = async () => {
  const cc = await characterClasses()
  const i = await items()
  const cci = await characterClassItems()

  const getItem = (id) => i.find((t) => id === t.id) || {}

  return cci.reduce((accumulator, currentValue) => {
    const c = accumulator.find((a) => a.id === currentValue.classId)
    c.items.push(getItem(currentValue.itemId))
    return accumulator
  }, cc.map((c) => ({ ...c, items: [] })))
}

const subject = new ReplaySubject()
subject.subscribe({
  next(x) { console.log({ x }) },
  error(err) { console.error({ err }) },
  complete() { console.error({ complete: 'complete' }) },
})

module.exports = {
  subject,
  addCharacterClassItem: addCharacterItem,
  getCharacters,
}

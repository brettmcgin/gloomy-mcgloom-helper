const { BehaviorSubject } = require('rxjs')
const { subject, getCharacters, addCharacterClassItem } = require('./repository')

const loadActionKey = 'loadAction'
const addItemActionKey = 'addItemAction'

const intents = new BehaviorSubject({ action: loadActionKey })

const loadIntent = async () => {
  intents.next({ action: loadActionKey })
}

const addCharacterClassIntent = async ({ itemId, classId }) => {
  intents.next({ action: addItemActionKey, data: { itemId, classId } })
}

const load = async () => subject.next(await getCharacters())
const addItem = async (data) => {
  await addCharacterClassItem(data)
  await loadIntent()
}

const handleActions = async ({ action, data, error }) => {
  console.log('handleAction')
  console.log({ action, data, error })
  if (action === loadActionKey) await load()
  if (action === addItemActionKey) await addItem(data)
  if (error) console.error(error)
}

intents.subscribe({
  next(i) { handleActions(i) },
  error(err) { console.error({ err }) },
  complete() { console.error({ complete: 'complete' }) },
})

module.exports = {
  addCharacterClassItem: addCharacterClassIntent,
  load: loadIntent,
}

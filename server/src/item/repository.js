const items = require('./data')

const all = async () => items

module.exports = {
  all,
}

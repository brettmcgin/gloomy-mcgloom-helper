const express = require('express')
const repository = require('./repository')

const router = express.Router()

router.get('/', async (req, res) => {
  res.json(await repository.all())
})

module.exports = router

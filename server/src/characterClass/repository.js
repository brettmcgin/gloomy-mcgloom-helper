module.exports = {
  all: async () => [
    { id: 1, abbreviation: 'TI', name: 'Tinkerer' },
    { id: 2, abbreviation: 'BR', name: 'Brute' },
    { id: 3, abbreviation: 'SC', name: 'Scoundrel' },
    { id: 4, abbreviation: 'SW', name: 'Spellweaver' },
    { id: 5, abbreviation: 'CH', name: 'Cragheart' },
    { id: 6, abbreviation: 'MT', name: 'Mindthief' },
    { id: 7, abbreviation: 'SK', name: 'Sunkeeper' },
    { id: 8, abbreviation: 'QM', name: 'Quartermaster' },
    { id: 9, abbreviation: 'SU', name: 'Summoner' },
    { id: 10, abbreviation: 'NS', name: 'Nightshroud' },
    { id: 11, abbreviation: 'PH', name: 'Plagueherald' },
    { id: 13, abbreviation: 'BE', name: 'Berserker' },
    { id: 14, abbreviation: 'SS', name: 'Soothsinger' },
    { id: 15, abbreviation: 'DS', name: 'Doomstalker' },
    { id: 16, abbreviation: 'SB', name: 'Sawbone' },
    { id: 17, abbreviation: 'EL', name: 'Elementalist' },
    { id: 18, abbreviation: 'BT', name: 'Beast Tyrant' },
    { id: 19, abbreviation: 'DR', name: 'Diviner' },
  ],
}

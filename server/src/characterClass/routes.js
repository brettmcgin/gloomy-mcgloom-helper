const express = require('express')
const repository = require('./repository')

const router = express.Router()

router.get('/', (req, res) => {
  res.json(repository.all())
})

module.exports = router

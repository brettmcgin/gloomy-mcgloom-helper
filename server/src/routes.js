const express = require('express')
const path = require('path')

const itemRoutes = require('./item/routes')
const characterClass = require('./characterClass/routes')
const characterRoutes = require('./character/routes')

module.exports = {
  route: (app) => {
    app.use('/', express.static(path.join(__dirname, '..', 'public')))
    app.use('/item', itemRoutes)
    app.use('/characterClass', characterClass)
    app.use('/character', characterRoutes)
  },
}

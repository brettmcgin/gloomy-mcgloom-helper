const express = require('express')
const bodyParser = require('body-parser')

const routes = require('./routes')

const app = express()
const port = 3001

app.use(bodyParser.json())

routes.route(app)

const server = app.listen(port, () => {
  console.log(`Example app listening at http://0.0.0.0:${port}`)
})

module.exports = server

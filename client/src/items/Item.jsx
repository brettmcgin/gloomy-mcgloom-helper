import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
  Card,
  CardContent,
  CardMedia,
  Typography
} from '@material-ui/core'

const useStyles = makeStyles(() => {
  const rootHeight = '12em'
  return {
    root: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: rootHeight
    },
    imageContainer: {
      minWidth: `calc(.66 * ${rootHeight})`,
      color: 'white',
      flexBasis: 'auto'
    },
    image: {
      height: '100%'
    },
    details: {
      flexGrow: 1,
      padding: '0',
      paddingLeft: '2em'
    },
    stats: {
      minWidth: '15%',
      'align-items': 'flex-start',
      borderLeft: '2px solid black'
    },
    itemContainer: {
      display: 'flex',
      flexDirection: 'row'
    },
    itemId: {
      color: 'red'
    }
  }
})

const Item = ({ item }) => {
  const classes = useStyles()

  return (
    <Card className={classes.root}>
      <div className={classes.imageContainer}>
        <CardMedia
          className={classes.image}
          image={item.image}
          title={item.name}
        />
      </div>

      <CardContent className={classes.details}>
        <Typography variant="h5" padding={0}>
          <div className={classes.itemContainer}>
            <div className={classes.temId}>{item.id}</div>
            <div>{item.name}</div>
          </div>
        </Typography>
        <Typography variant="body1">{item.desc}</Typography>
        {item.faq && <Typography variant="body2">{item.faq}</Typography>}
      </CardContent>

      <div className={classes.stats}>
        <Typography variant="body1" className={classes.statsRow}>{`Cost: ${item.cost}`}</Typography>
        <Typography variant="body1" className={classes.statsRow}>{`Stock: ${item.count} / ${item.count}`}</Typography>
        {item.slot && <Typography variant="body1" className={classes.statsRow}>{`Cost: ${item.cost}`}</Typography>}
      </div>
    </Card>
  )
}

Item.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    cost: PropTypes.number,
    count: PropTypes.number,
    slot: PropTypes.string,
    spent: PropTypes.bool,
    desc: PropTypes.string,
    faq: PropTypes.string
  }).isRequired
}

export default Item

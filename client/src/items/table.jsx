import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { TextField } from '@material-ui/core'

import imageMap from './imageMap'
import Items from './Items'

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  filters: {
    display: 'flex',
    flexDirection: 'row'
  }
}))

export default () => {
  const classes = useStyles()

  const [data, setData] = useState([])
  const [search, setSearch] = useState('')

  useEffect(() => {
    fetch('/item')
      .then(response => response.json())
      .then(response => response.map(d => ({ ...d, image: imageMap.find(i => i.id === d.id)?.image || '' })))
      .then(response => setData(response))
  }, [])

  const searchHandler = s => { setSearch(s) }

  const items = () => data
    .filter(d => d.desc.includes(search))

  return (
    <div className={classes.root}>
      <div className={classes.filters}>
        <TextField id="outlined-basic" label="Search" variant="outlined" onChange={async event => { searchHandler(event?.target?.value) }} />
      </div>
      <div>
        <Items items={items()} />
      </div>
    </div>
  )
}

module.exports = [
  {
    id: 1,
    image: './img/items/boots-of-striding.png'
  },
  {
    id: 2,
    image: './img/items/winged-shoes.png'
  },
  {
    id: 3,
    image: './img/items/hide-armor.png'
  },
  {
    id: 4,
    image: './img/items/leather-armor.png'
  },
  {
    id: 5,
    image: './img/items/cloak-of-invisibility.png'
  },
  {
    id: 6,
    image: './img/items/eagle-eye-goggles.png'
  },
  {
    id: 7,
    image: './img/items/iron-helmet.png'
  },
  {
    id: 8,
    image: './img/items/heater-shield.png'
  },
  {
    id: 9,
    image: './img/items/piercing-bow.png'
  },
  {
    id: 10,
    image: './img/items/war-hammer.png'
  },
  {
    id: 11,
    image: './img/items/poison-dagger.png'
  },
  {
    id: 12,
    image: './img/items/minor-healing-potion.png'
  },
  {
    id: 13,
    image: './img/items/minor-stamina-potion.png'
  },
  {
    id: 14,
    image: './img/items/minor-power-potion.png'
  },
  {
    id: 15,
    image: './img/items/boots-of-speed.png'
  },
  {
    id: 16,
    image: './img/items/cloak-of-pockets.png'
  },
  {
    id: 17,
    image: './img/items/empowering-talisman.png'
  },
  {
    id: 18,
    image: './img/items/battle-axe.png'
  },
  {
    id: 19,
    image: './img/items/weighted-net.png'
  },
  {
    id: 20,
    image: './img/items/minor-mana-potion.png'
  },
  {
    id: 21,
    image: './img/items/stun-powder.png'
  },
  {
    id: 22,
    image: './img/items/heavy-greaves.png'
  },
  {
    id: 23,
    image: './img/items/chainmail.png'
  },
  {
    id: 24,
    image: './img/items/amulet-of-life.png'
  },
  {
    id: 25,
    image: './img/items/jagged-sword.png'
  },
  {
    id: 26,
    image: './img/items/long-spear.png'
  },
  {
    id: 27,
    image: './img/items/major-healing-potion.png'
  },
  {
    id: 28,
    image: './img/items/moon-earring.png'
  },
  {
    id: 29,
    image: './img/items/comfortable-shoes.png'
  },
  {
    id: 30,
    image: './img/items/studded-leather.png'
  },
  {
    id: 31,
    image: './img/items/hawk-helm.png'
  },
  {
    id: 32,
    image: './img/items/tower-shield.png'
  },
  {
    id: 33,
    image: './img/items/volatile-bomb.png'
  },
  {
    id: 34,
    image: './img/items/major-stamina-potion.png'
  },
  {
    id: 35,
    image: './img/items/falcon-figurine.png'
  },
  {
    id: 36,
    image: './img/items/boots-of-dashing.png'
  },
  {
    id: 37,
    image: './img/items/robes-of-evocation.png'
  },
  {
    id: 38,
    image: './img/items/heavy-basinet.png'
  },
  {
    id: 39,
    image: './img/items/hooked-chain.png'
  },
  {
    id: 40,
    image: './img/items/versatile-dagger.png'
  },
  {
    id: 41,
    image: './img/items/major-power-potion.png'
  },
  {
    id: 42,
    image: './img/items/ring-of-haste.png'
  },
  {
    id: 43,
    image: './img/items/boots-of-quickness.png'
  },
  {
    id: 44,
    image: './img/items/splintmail.png'
  },
  {
    id: 45,
    image: './img/items/pendant-of-dark-pacts.png'
  },
  {
    id: 46,
    image: './img/items/spiked-shield.png'
  },
  {
    id: 47,
    image: './img/items/reaping-scythe.png'
  },
  {
    id: 48,
    image: './img/items/major-mana-potion.png'
  },
  {
    id: 49,
    image: './img/items/sun-earring.png'
  },
  {
    id: 50,
    image: './img/items/steel-sabatons.png'
  },
  {
    id: 51,
    image: './img/items/shadow-armor.png'
  },
  {
    id: 52,
    image: './img/items/protective-charm.png'
  },
  {
    id: 53,
    image: './img/items/black-knife.png'
  },
  {
    id: 54,
    image: './img/items/staff-of-eminence.png'
  },
  {
    id: 55,
    image: './img/items/super-healing-potion.png'
  },
  {
    id: 56,
    image: './img/items/ring-of-brutality.png'
  },
  {
    id: 57,
    image: './img/items/serene-sandals.png'
  },
  {
    id: 58,
    image: './img/items/cloak-of-phasing.png'
  },
  {
    id: 59,
    image: './img/items/telescopic-lens.png'
  },
  {
    id: 60,
    image: './img/items/unstable-explosives.png'
  },
  {
    id: 61,
    image: './img/items/wall-shield.png'
  },
  {
    id: 62,
    image: './img/items/doom-powder.png'
  },
  {
    id: 63,
    image: './img/items/lucky-eye.png'
  },
  {
    id: 64,
    image: './img/items/boots-of-sprinting.png'
  },
  {
    id: 65,
    image: './img/items/platemail.png'
  },
  {
    id: 66,
    image: './img/items/mask-of-terror.png'
  },
  {
    id: 67,
    image: './img/items/balanced-blade.png'
  },
  {
    id: 68,
    image: './img/items/halberd.png'
  },
  {
    id: 69,
    image: './img/items/star-earring.png'
  },
  {
    id: 70,
    image: './img/items/second-chance-ring.png'
  },
  {
    id: 71,
    image: './img/items/boots-of-levitation.png'
  },
  {
    id: 72,
    image: './img/items/shoes-of-happiness.png'
  },
  {
    id: 73,
    image: './img/items/blinking-cape.png'
  },
  {
    id: 74,
    image: './img/items/swordedge-armor.png'
  },
  {
    id: 75,
    image: './img/items/circlet-of-elements.png'
  },
  {
    id: 76,
    image: './img/items/chain-hood.png'
  },
  {
    id: 77,
    image: './img/items/frigid-blade.png'
  },
  {
    id: 78,
    image: './img/items/storm-blade.png'
  },
  {
    id: 79,
    image: './img/items/inferno-blade.png'
  },
  {
    id: 80,
    image: './img/items/tremor-blade.png'
  },
  {
    id: 81,
    image: './img/items/brilliant-blade.png'
  },
  {
    id: 82,
    image: './img/items/night-blade.png'
  },
  {
    id: 83,
    image: './img/items/wand-of-frost.png'
  },
  {
    id: 84,
    image: './img/items/wand-of-storms.png'
  },
  {
    id: 85,
    image: './img/items/wand-of-infernos.png'
  },
  {
    id: 86,
    image: './img/items/wand-of-tremors.png'
  },
  {
    id: 87,
    image: './img/items/wand-of-brilliance.png'
  },
  {
    id: 88,
    image: './img/items/wand-of-darkness.png'
  },
  {
    id: 89,
    image: './img/items/minor-cure-potion.png'
  },
  {
    id: 90,
    image: './img/items/major-cure-potion.png'
  },
  {
    id: 91,
    image: './img/items/steel-ring.png'
  },
  {
    id: 92,
    image: './img/items/dampening-ring.png'
  },
  {
    id: 93,
    image: './img/items/scroll-of-power.png'
  },
  {
    id: 94,
    image: './img/items/scroll-of-healing.png'
  },
  {
    id: 95,
    image: './img/items/scroll-of-stamina.png'
  },
  {
    id: 96,
    image: './img/items/rocket-boots.png'
  },
  {
    id: 97,
    image: './img/items/endurance-footwraps.png'
  },
  {
    id: 98,
    image: './img/items/drakescale-boots.png'
  },
  {
    id: 99,
    image: './img/items/magma-waders.png'
  },
  {
    id: 100,
    image: './img/items/robes-of-summoning.png'
  },
  {
    id: 101,
    image: './img/items/second-skin.png'
  },
  {
    id: 102,
    image: './img/items/sacrificial-robes.png'
  },
  {
    id: 103,
    image: './img/items/drakescale-armor.png'
  },
  {
    id: 104,
    image: './img/items/steam-armor.png'
  },
  {
    id: 105,
    image: './img/items/flea-bitten-shawl.png'
  },
  {
    id: 106,
    image: './img/items/necklace-of-teeth.png'
  },
  {
    id: 107,
    image: './img/items/horned-helm.png'
  },
  {
    id: 108,
    image: './img/items/drakescale-helm.png'
  },
  {
    id: 109,
    image: './img/items/thiefs-hood.png'
  },
  {
    id: 110,
    image: './img/items/helm-of-the-mountain.png'
  },
  {
    id: 111,
    image: './img/items/wave-crest.png'
  },
  {
    id: 112,
    image: './img/items/ancient-drill.png'
  },
  {
    id: 113,
    image: './img/items/skullbane-axe.png'
  },
  {
    id: 114,
    image: './img/items/staff-of-xorn.png'
  },
  {
    id: 115,
    image: './img/items/mountain-hammer.png'
  },
  {
    id: 116,
    image: './img/items/fueled-falchion.png'
  },
  {
    id: 117,
    image: './img/items/bloody-axe.png'
  },
  {
    id: 118,
    image: './img/items/staff-of-elements.png'
  },
  {
    id: 119,
    image: './img/items/skull-of-hatred.png'
  },
  {
    id: 120,
    image: './img/items/staff-of-summoning.png'
  },
  {
    id: 121,
    image: './img/items/orb-of-dawn.png'
  },
  {
    id: 122,
    image: './img/items/orb-of-twilight.png'
  },
  {
    id: 123,
    image: './img/items/ring-of-skulls.png'
  },
  {
    id: 124,
    image: './img/items/doomed-compass.png'
  },
  {
    id: 125,
    image: './img/items/curious-gear.png'
  },
  {
    id: 126,
    image: './img/items/remote-spider.png'
  },
  {
    id: 127,
    image: './img/items/giant-remote-spider.png'
  },
  {
    id: 128,
    image: './img/items/black-censer.png'
  },
  {
    id: 129,
    image: './img/items/black-card.png'
  },
  {
    id: 130,
    image: './img/items/helix-ring.png'
  },
  {
    id: 131,
    image: './img/items/heart-of-the-betrayer.png'
  },
  {
    id: 132,
    image: './img/items/power-core.png'
  },
  {
    id: 133,
    image: './img/items/resonant-crystal.png'
  },
  {
    id: 134,
    image: './img/items/imposing-blade.png'
  },
  {
    id: 135,
    image: './img/items/focusing-ray.png'
  },
  {
    id: 136,
    image: './img/items/volatile-elixir.png'
  },
  {
    id: 137,
    image: './img/items/silent-stiletto.png'
  },
  {
    id: 138,
    image: './img/items/stone-charm.png'
  },
  {
    id: 139,
    image: './img/items/psychic-knife.png'
  },
  {
    id: 140,
    image: './img/items/sun-shield.png'
  },
  {
    id: 141,
    image: './img/items/utility-belt.png'
  },
  {
    id: 142,
    image: './img/items/phasing-idol.png'
  },
  {
    id: 143,
    image: './img/items/smoke-elixir.png'
  },
  {
    id: 144,
    image: './img/items/pendant-of-the-plague.png'
  },
  {
    id: 145,
    image: './img/items/mask-of-death.png'
  },
  {
    id: 146,
    image: './img/items/masters-lute.png'
  },
  {
    id: 147,
    image: './img/items/cloak-of-the-hunter.png'
  },
  {
    id: 148,
    image: './img/items/doctors-coat.png'
  },
  {
    id: 149,
    image: './img/items/elemental-boots.png'
  },
  {
    id: 150,
    image: './img/items/staff-of-command.png'
  },
  {
    id: 152,
    image: './img/items/ring-of-duality.png'
  },
  {
    id: 153,
    image: './img/items/minor-antidote.png'
  },
  {
    id: 154,
    image: './img/items/major-antidote.png'
  },
  {
    id: 155,
    image: './img/items/curseward-armor.png'
  },
  {
    id: 156,
    image: './img/items/elemental-claymore.png'
  },
  {
    id: 157,
    image: './img/items/ancient-bow.png'
  },
  {
    id: 158,
    image: './img/items/rejuvenation-greaves.png'
  },
  {
    id: 159,
    image: './img/items/scroll-of-haste.png'
  },
  {
    id: 160,
    image: './img/items/cutpurse-dagger.png'
  },
  {
    id: 161,
    image: './img/items/throwing-axe.png'
  },
  {
    id: 162,
    image: './img/items/rift-device.png'
  },
  {
    id: 163,
    image: './img/items/crystal-tiara.png'
  },
  {
    id: 164,
    image: './img/items/basin-of-prophecy.png'
  }
]

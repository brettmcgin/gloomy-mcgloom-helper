import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { Grid } from '@material-ui/core'

import Item from './Item'

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    width: '100%'
  }
}))

const Items = ({ items }) => {
  const classes = useStyles()

  const itemCards = items.map(i => (
    <Grid item key={i.id} xs={12} md={3}>
      <Item item={i} key={i.id} />
    </Grid>
  ))

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        {itemCards}
      </Grid>
    </div>
  )
}

Items.defaultProps = {
  items: []
}

Items.propTypes = {
  items: PropTypes.arrayOf(Item.propTypes.item)
}

export default Items

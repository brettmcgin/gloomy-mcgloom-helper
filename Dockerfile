FROM node:latest

WORKDIR /app

COPY client/package*.json client/
COPY server/package*.json server/

RUN (cd client; npm i)
RUN (cd server; npm i)

COPY . .

RUN (cd client; npm run build)

RUN mv client/build server/public

CMD node server/src/server.js
